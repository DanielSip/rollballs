﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CollisionScript : MonoBehaviour
{
    private Rigidbody playerRigidbody;
    public float boomspeed;
    

    // Start is called before the first frame update
    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.Equals("Player"))
        {
            
            Rigidbody rig = collision.gameObject.GetComponent<Rigidbody>();
            rig.AddForce(rig.velocity * -boomspeed, ForceMode.Impulse);
        }
        
        
    }
}
