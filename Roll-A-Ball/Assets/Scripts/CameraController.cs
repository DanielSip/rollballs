﻿using UnityEngine;

public class CameraController : MonoBehaviour {

	public GameObject playerGameObject;
	public Vector3 offset;

	void Start () {
		offset = transform.position - playerGameObject.transform.position;
	}
	
	void LateUpdate () {
		transform.position = playerGameObject.transform.position + offset;
        transform.LookAt(playerGameObject.transform);
    }
}
