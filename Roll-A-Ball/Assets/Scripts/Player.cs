﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

	public float speed;
	public Text countText;
	public Text winText;

	private Rigidbody playerRigidbody;
	public int count;
	public int numberOfCubes;

	void Start ()
	{
		playerRigidbody = GetComponent<Rigidbody>();
		count = 0;
		numberOfCubes = GameObject.FindGameObjectsWithTag ("cube").Length;
		SetTexts ();
	}

	void FixedUpdate ()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		playerRigidbody.AddForce (movement * speed);
	}

	void OnTriggerEnter(Collider collider) 
	{
		if (collider.gameObject.CompareTag ("cube"))
		{
			Destroy(collider.gameObject);
			count++;
			SetTexts ();
		}
	}

	void SetTexts()
	{
		countText.text = "Punkte: " + count.ToString ();
		if (count >= numberOfCubes)
		{
			winText.text = "Gewonnen!";
			winText.gameObject.SetActive (true);
            Invoke("BackToMenu", 2.0f);
		}
	}

    public void BackToMenu() {
        SceneManager.LoadSceneAsync("MenuScene");
    }

}