﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

    public string sceneName;

	public void StartButtonPressed () {
        SceneManager.LoadSceneAsync(sceneName);
	}
	
}
